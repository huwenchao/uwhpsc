
.. _computing_options:

================================
Computing Options [2014 version]
================================

All of the software we will use this year is open source, so in principle
you can install it all on your own computer.  See `installing_software` for
some tips on doing so.

However, there are several reasons you might want to use a different
computing environment for this class:

* To avoid having to install many packages yourself,
* To make sure you have the same computing environment as fellow students 
  and the TAs,
* To have access to a multi-core machine if your own computer is old,

.. _options_smc:

SageMathCloud
--------------

This is the preferred computing platform for 

